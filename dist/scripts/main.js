'use strict';

$(document).ready(function () {

  $('.i-cases').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    speed: 300
  });

  $('.e-gallery').slick({
    arrows: true,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 1199,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true
      }
    }]
  });

  $('.phone').mask('+7 (999) 999-99-99');

  $('form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize()
    }).done(function (data) {
      if (data.response == 'ok') {
        /* alert('Спасибо! Ваша заявка отправлена'); */
        window.location.replace('./order.php');
      } else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

  var timer;

  var compareDate = new Date();
  compareDate.setDate(compareDate.getDate() + 7); //just for this demo today + 7 days

  timer = setInterval(function () {
    timeBetweenDates(compareDate);
  }, 1000);

  function timeBetweenDates(toDate) {
    var dateEntered = toDate;
    var now = new Date();
    var difference = dateEntered.getTime() - now.getTime();

    if (difference <= 0) {

      // Timer done
      clearInterval(timer);
    } else {

      var seconds = Math.floor(difference / 1000);
      var minutes = Math.floor(seconds / 60);
      var hours = Math.floor(minutes / 60);
      var days = Math.floor(hours / 24);

      hours %= 24;
      minutes %= 60;
      seconds %= 60;

      $('#hours').text(hours);
      $('#minutes').text(minutes);
      $('#seconds').text(seconds);

      $('#hours_2').text(hours);
      $('#minutes_2').text(minutes);
      $('#seconds_2').text(seconds);
    }
  }
});