<?php
require_once('leadvertex.class.php');

$referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
$remote_ip = empty($_SERVER['REMOTE_ADDR']) ? '' : $_SERVER['REMOTE_ADDR'];
$phone = empty($_POST['phone']) ? '' : $_POST['phone'];
$name = empty($_POST['name']) ? 'Уточнить у клиента' : $_POST['name'];
$price = empty($_POST['price']) ? '0' : $_POST['price'];
$comment = empty($_POST['comment']) ? 'EPIL' : $_POST['comment'];
$offer = 'EPIL';

$temp = parse_url($referer);
$domain = isset($temp['host']) ? $temp['host'] : 'NO DOMAIN GET';

$config = array(
	'' => array('id' => '1', 'key' => 'd64e44c1769404e054b6df18936105d5'),
);

$user_id = isset($config[$domain]['id']) ? $config[$domain]['id'] : '1';
$api_key = isset($config[$domain]['key']) ? $config[$domain]['key'] : 'd64e44c1769404e054b6df18936105d5';

$lv = new Leadvertex('demo-5342', $user_id, $api_key);
$order_id = $lv->add_lead($referer, $remote_ip, $phone, $name, $price, $comment, $offer);

$error = '';	# empty($lv->error) ? '' : $lv->error;

if($order_id) {
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<link type="text/css" rel="stylesheet" href="./styles/thx.css" />
	<title>Заявка отправленна</title>
</head>
<body>
	<div class="wrap_block_success">
		<div class="block_success">
			<h2>ПОЗДРАВЛЯЕМ! ВАШ ЗАКАЗ № <?= $order_id ?> ПРИНЯТ!</h2>
			<p class="success">В ближайшее время с вами свяжется оператор для подтверждения заказа. Пожалуйста, включите ваш контактный телефон.</p>
			<h3>Пожалуйста, проверьте правильность введенной вами информации</h3>
			<p class="fail">Если вы ошиблись при заполнени формы, то, пожалуйста, <a href="<?php echo $referer; ?>">заполните заявку еще раз</a></p>
		</div>
	</div>
</body>
</html>

<?php
} else {
?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<link type="text/css" rel="stylesheet" href="css/style_tnx_pg.css" />
	<title>Заявка отправленна</title>
</head>
<body>
	<div class="wrap_block_success">
		<div class="block_success">
			<h2>ПОЗДРАВЛЯЕМ! ВАШ ЗАКАЗ № <?= $order_id ?> ПРИНЯТ!</h2>
			<p class="success">В ближайшее время с вами свяжется оператор для подтверждения заказа. Пожалуйста, включите ваш контактный телефон.</p>
			<h3>Пожалуйста, проверьте правильность введенной вами информации</h3>
			<p class="fail">Если вы ошиблись при заполнени формы, то, пожалуйста, <a href="<?php echo $referer; ?>">заполните заявку еще раз</a></p>
		</div>
	</div>
</body>
</html>
<?php
}
